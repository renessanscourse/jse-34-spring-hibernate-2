package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.Result;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ISessionEndpoint {

    @NotNull
    @WebMethod
    SessionDTO openSession(
            @WebParam(name =  "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    ) throws AccessForbiddenException;

    @NotNull
    @WebMethod
    Result closeSession(
            @WebParam(name = "session", partName = "session") SessionDTO sessionDTO
    ) throws AccessForbiddenException;

    @NotNull
    @WebMethod
    List<SessionDTO> sessionsOfUser(
            @WebParam(name = "session", partName = "session") SessionDTO sessionDTO
    );

    @WebMethod
    boolean isValid(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO
    );

    @WebMethod
    void signOutByLogin(@Nullable @WebParam(name = "login", partName = "login") String login);

    @WebMethod
    void signOutByUserId(@Nullable @WebParam(name = "userId", partName = "userId") String userId);

    @WebMethod
    @NotNull UserDTO getUser(@Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO) throws AccessForbiddenException;

    @WebMethod
    void closeAll(@Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO) throws AccessForbiddenException;

}