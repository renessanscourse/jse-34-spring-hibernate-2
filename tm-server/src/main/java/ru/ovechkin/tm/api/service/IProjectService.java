package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    @Transactional
    void add(@Nullable SessionDTO sessionDTO, @Nullable ProjectDTO project);

    void create(@Nullable SessionDTO sessionDTO, @Nullable String name);

    void create(@Nullable SessionDTO sessionDTO, @Nullable  String name, @Nullable String description);

    @Nullable
    List<ProjectDTO> findUserProjects(@Nullable String userId);

    @Transactional
    void removeAllUserProjects(@Nullable String userId);

    @NotNull
    ProjectDTO findProjectById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO findProjectByName(@Nullable String userId, @Nullable String name);

    @NotNull
    @Transactional
    ProjectDTO updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    @Transactional
    ProjectDTO removeProjectById(@Nullable String userId, @Nullable String id);

    @Nullable
    @Transactional
    ProjectDTO removeProjectByName(@Nullable String userId, @Nullable String name);

    @NotNull List<ProjectDTO> getAllProjectsDTO();

    @Transactional
    @NotNull List<ProjectDTO> loadProjects(@Nullable List<ProjectDTO> projectsDTO);

    @Transactional
    void removeAllProjects();

}