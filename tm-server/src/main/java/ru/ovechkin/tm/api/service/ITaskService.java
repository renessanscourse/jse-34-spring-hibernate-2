package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.TaskDTO;
import ru.ovechkin.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    @Transactional
    void add(
            @Nullable SessionDTO sessionDTO,
            @Nullable TaskDTO taskDTO,
            @Nullable ProjectDTO projectDTO
    );

    void create(
            @Nullable SessionDTO sessionDTO,
            @Nullable String taskName,
            @Nullable String projectId
    );

    void create(
            @Nullable SessionDTO sessionDTO,
            @Nullable String taskName,
            @Nullable String taskDescription,
            @Nullable String projectId
    );

    @Nullable
    List<TaskDTO> findUserTasks(@Nullable String userId);

    @Transactional
    void removeAllUserTasks(@Nullable String userId);

    TaskDTO findTaskById(@Nullable String userId, @Nullable String id);

    TaskDTO findTaskByName(@Nullable String userId, @Nullable String name);

    @NotNull
    @Transactional
    TaskDTO updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    @Transactional
    TaskDTO removeTaskById(@Nullable String userId, @Nullable String id);

    @Transactional
    TaskDTO removeTaskByName(@Nullable String userId, @Nullable String name);

    @NotNull List<TaskDTO> getAllTasksDTO();

    @Transactional
    @NotNull List<TaskDTO> loadTasks(@Nullable List<TaskDTO> tasksDTO);

    @Transactional
    void removeAllTasks();

}