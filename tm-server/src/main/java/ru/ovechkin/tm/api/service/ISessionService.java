package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;

import java.util.List;

public interface ISessionService {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    boolean isValid(@Nullable SessionDTO sessionDTO);

    @Transactional
    @NotNull SessionDTO open(@Nullable String login, @Nullable String password);

    void validate(@Nullable SessionDTO sessionDTO);

    void validate(@Nullable SessionDTO sessionDTO, @Nullable Role role);

    @NotNull SessionDTO sign(@Nullable SessionDTO sessionDTO);

    @Transactional
    void signOutByLogin(@Nullable String login);

    @Transactional
    void signOutByUserId(@Nullable String userId);

    @NotNull UserDTO getUser(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

    @NotNull String getUserId(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

    @NotNull List<SessionDTO> getListSession(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

    @Transactional
    void close(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

    @Transactional
    void closeAll(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

}