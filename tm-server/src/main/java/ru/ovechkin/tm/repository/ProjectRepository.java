package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    List<Project> findByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    Project findProjectByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    Project findProjectByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    void deleteProjectByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteProjectByUserIdAndName(@NotNull final String userId, @NotNull final String name);

}