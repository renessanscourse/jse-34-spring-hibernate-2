package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ovechkin.tm.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    List<Task> findByUserId(@NotNull String userId);

    void deleteAllByUserId(@NotNull final String userId);

    Task findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    Task findByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String name);

}