package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.ovechkin.tm.api.endpoint.IStorageEndpoint;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.service.IStorageService;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.service.PropertyService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.*;

@Controller
@WebService
public class StorageEndpoint extends AbstractEndpoint implements IStorageEndpoint {

    @Autowired
    private IStorageService storageService;

    @Autowired
    private ISessionService sessionService;

    public StorageEndpoint() {
    }

    @Override
    @WebMethod
    public void dataBase64Save(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataBase64Save();
    }

    @Override
    @WebMethod
    public void dataBase64Load(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataBase64Load();
    }

    @Override
    @WebMethod
    public void dataBinarySave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataBinarySave();
    }

    @Override
    @WebMethod
    public void dataBinaryLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException, ClassNotFoundException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataBinaryLoad();
    }

    @Override
    @WebMethod
    public void dataJsonJaxbSave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException, JAXBException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataJsonJaxbSave();
    }

    @Override
    @WebMethod
    public void dataJsonJaxbLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws JAXBException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataJsonJaxbLoad();
    }

    @Override
    @WebMethod
    public void dataJsonMapperLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataJsonMapperLoad();
    }

    @Override
    @WebMethod
    public void dataJsonMapperSave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataJsonMapperSave();
    }

    @Override
    @WebMethod
    public void dataXmlJaxbSave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException, JAXBException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataXmlJaxbSave();
    }

    @Override
    @WebMethod
    public void dataXmlJaxbLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException, JAXBException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataXmlJaxbLoad();
    }

    @Override
    @WebMethod
    public void dataXmlMapperSave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataXmlMapperSave();
    }

    @Override
    @WebMethod
    public void dataXmlMapperLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        sessionService.validate(sessionDTO, Role.ADMIN);
        storageService.dataXmlMapperLoad();
    }

    @NotNull
    @Override
    @WebMethod
    public Integer getServerPortInfo() {
        return context.getBean(PropertyService.class).getServicePort();
    }

    @NotNull
    @Override
    @WebMethod
    public String getServerHostInfo() {
        return context.getBean(PropertyService.class).getServiceHost();
    }

}