package ru.ovechkin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public abstract class AbstractService {

    @Autowired
    protected AnnotationConfigApplicationContext context;

}