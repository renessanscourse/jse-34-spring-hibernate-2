package ru.ovechkin.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ovechkin.tm.api.service.*;
import ru.ovechkin.tm.dto.Domain;

@Service
public class DomainService implements IDomainService {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        projectService.loadProjects(domain.getProjects());
        taskService.loadTasks(domain.getTasks());
        userService.loadUsers(domain.getUsers());
    }

    @Override
    public void save(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getAllProjectsDTO());
        domain.setTasks(taskService.getAllTasksDTO());
        domain.setUsers(userService.getAllUsersDTO());
    }

}