package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ovechkin.tm.api.service.IPropertyService;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.exeption.user.SessionsEmptyListException;
import ru.ovechkin.tm.repository.SessionRepository;
import ru.ovechkin.tm.util.HashUtil;
import ru.ovechkin.tm.util.SignatureUtil;

import java.util.ArrayList;
import java.util.List;

@Service
public class SessionService extends AbstractService implements ISessionService {

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private IUserService userService;

    public SessionService() {
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserDTO userDTO = userService.findByLogin(login);
        if (userDTO == null) return false;
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(userDTO.getPasswordHash());
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO sessionDTO) {
        try {
            validate(sessionDTO);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO) throws AccessForbiddenException {
        if (sessionDTO == null) throw new AccessForbiddenException();
        if (sessionDTO.getSignature() == null || sessionDTO.getSignature().isEmpty())
            throw new AccessForbiddenException();
        if (sessionDTO.getUserId() == null || sessionDTO.getUserId().isEmpty()) throw new AccessForbiddenException();
        if (sessionDTO.getTimestamp() == null) throw new AccessForbiddenException();
        @Nullable final SessionDTO temp = sessionDTO.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = sessionDTO.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
        if (!sessionRepository.findById(sessionDTO.getId()).isPresent()) throw new AccessForbiddenException();
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO, @Nullable final Role role) {
        if (role == null) throw new AccessForbiddenException();
        validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @Nullable final UserDTO userDTO = userService.findById(userId);
        if (userDTO == null) throw new AccessForbiddenException();
        if (userDTO.getRole() == null) throw new AccessForbiddenException();
        if (!role.equals(userDTO.getRole())) throw new AccessForbiddenException();
    }

    @NotNull
    @Override
    public SessionDTO open(@Nullable final String login, @Nullable final String password) {
        if (!checkDataAccess(login, password)) throw new AccessDeniedException();
        @Nullable final UserDTO userDTO = userService.findByLogin(login);
        if (userDTO == null) throw new UserDoesNotExistException();
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(userDTO.getId());
        sessionDTO.setTimestamp(System.currentTimeMillis());
        sign(sessionDTO);
        @NotNull final Session session = getSessionAsEntity(sessionDTO, userDTO);
        sessionRepository.save(session);
        return sessionDTO;
    }

    @NotNull
    @Override
    public SessionDTO sign(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) throw new AccessForbiddenException();
        sessionDTO.setSignature("");
        @NotNull final IPropertyService propertyService = context.getBean(PropertyService.class);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @NotNull final String signature = SignatureUtil.sign(sessionDTO, salt, cycle);
        sessionDTO.setSignature(signature);
        return sessionDTO;
    }

    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDTO userDTO = userService.findByLogin(login);
        if (userDTO == null) throw new UserDoesNotExistException();
        @NotNull final String userId = userDTO.getId();
        sessionRepository.deleteAllByUserId(userId);
    }

    public void signOutByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @Nullable final UserDTO userDTO = userService.findById(userId);
        if (userDTO == null) return;
        sessionRepository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public UserDTO getUser(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException {
        @NotNull final String userId = getUserId(sessionDTO);
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException {
        validate(sessionDTO);
        return sessionDTO.getUserId();
    }

    @NotNull
    @Override
    public List<SessionDTO> getListSession(@Nullable final SessionDTO sessionDTO) throws AccessForbiddenException {
        validate(sessionDTO);
        @Nullable final List<Session> sessions = sessionRepository.findByUserId(sessionDTO.getUserId());
        if (sessions == null || sessions.isEmpty()) throw new SessionsEmptyListException();
        @NotNull final List<SessionDTO> sessionsDTO = new ArrayList<>();
        for (@NotNull final Session session : sessions) {
            SessionDTO sessionDTOForList = new SessionDTO();
            sessionDTOForList.setId(session.getId());
            sessionDTOForList.setUserId(session.getUser().getId());
            sessionDTOForList.setSignature(session.getSignature());
            sessionDTOForList.setTimestamp(session.getCreationTime());
            sessionsDTO.add(sessionDTO);
        }
        return sessionsDTO;
    }

    @Override
    public void close(@Nullable final SessionDTO sessionDTO) throws AccessForbiddenException {
        validate(sessionDTO);
        remove(sessionDTO);
    }

    @Nullable
    public SessionDTO remove(@Nullable final SessionDTO sessionDTO) {
        validate(sessionDTO);
        @Nullable final UserDTO userDTO = userService.findById(sessionDTO.getId());
        if (userDTO == null) throw new UserDoesNotExistException();
        @NotNull final Session session = getSessionAsEntity(sessionDTO, userDTO);
        sessionRepository.delete(session);
        return sessionDTO;
    }

    @NotNull
    private Session getSessionAsEntity(@NotNull SessionDTO sessionDTO, @Nullable UserDTO userDTO) {
        @NotNull final Session session = new Session();
        session.setId(sessionDTO.getId());
        @NotNull final User user = new User(userDTO);
        session.setUser(user);
        session.setRole(user.getRole());
        session.setCreationTime(sessionDTO.getTimestamp());
        session.setSignature(sessionDTO.getSignature());
        return session;
    }

    @Override
    public void closeAll(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException {
        validate(sessionDTO);
        sessionRepository.deleteAllByUserId(sessionDTO.getUserId());
    }

}