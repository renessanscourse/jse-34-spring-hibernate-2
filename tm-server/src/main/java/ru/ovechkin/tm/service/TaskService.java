package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.TaskDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.exeption.other.NameAlreadyTakenException;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.unknown.TaskUnknownException;
import ru.ovechkin.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService extends AbstractService implements ITaskService {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Transactional
    public void add(
            @Nullable final SessionDTO sessionDTO,
            @Nullable final TaskDTO taskDTO,
            @Nullable final ProjectDTO projectDTO
    ) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (taskDTO == null) return;
        if (projectDTO == null) return;
        @NotNull final UserDTO userDTO = context.getBean(ISessionService.class).getUser(sessionDTO);
        @NotNull final User user = new User(userDTO);
        @NotNull final Task task = new Task(taskDTO);
        @NotNull final Project project = new Project(projectDTO);
        task.setUser(user);
        task.setProject(project);
        taskRepository.save(task);
    }

    @Override
    public void create(
            @Nullable final SessionDTO sessionDTO,
            @Nullable final String taskName,
            @Nullable final String projectId
    ) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (taskName == null || taskName.isEmpty()) throw new NameEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName(taskName);
        @NotNull final ProjectDTO projectDTO = projectService.findProjectById(sessionDTO.getUserId(), projectId);
        add(sessionDTO, taskDTO, projectDTO);
    }

    @Override
    public void create(
            @Nullable final SessionDTO sessionDTO,
            @Nullable final String taskName,
            @Nullable final String taskDescription,
            @Nullable final String projectId
    ) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (taskName == null || taskName.isEmpty()) throw new NameEmptyException();
        if (taskDescription == null || taskDescription.isEmpty()) throw new DescriptionEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName(taskName);
        taskDTO.setDescription(taskDescription);
        @NotNull final ProjectDTO projectDTO = projectService.findProjectById(sessionDTO.getUserId(), projectId);
        add(sessionDTO, taskDTO, projectDTO);
    }

    @Nullable
    @Override
    public List<TaskDTO> findUserTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final List<Task> tasks = taskRepository.findByUserId(userId);
        if (tasks == null || tasks.isEmpty()) return null;
        return getTaskDTOS(tasks);
    }

    @NotNull
    private List<TaskDTO> getTaskDTOS(@Nullable List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TaskUnknownException();
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            @NotNull final TaskDTO taskDTO = new TaskDTO(task);
            tasksDTO.add(taskDTO);
        }
        return tasksDTO;
    }

    @Override
    @Transactional
    public void removeAllUserTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        taskRepository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public TaskDTO findTaskById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskUnknownException();
        return new TaskDTO(task);
    }

    @Nullable
    @Override
    public TaskDTO findTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = taskRepository.findByUserIdAndName(userId, name);
        if (task == null) throw new TaskUnknownException(name);
        return new TaskDTO(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (taskRepository.findByUserIdAndName(userId, name) != null)
            throw new NameAlreadyTakenException(name);
        @Nullable final Task task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskUnknownException();
        task.setName(name);
        task.setDescription(description);
        return new TaskDTO(task);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO removeTaskById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskUnknownException();
        taskRepository.deleteByUserIdAndId(userId, id);
        return new TaskDTO(task);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO removeTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = taskRepository.findByUserIdAndName(userId, name);
        if (task == null) throw new TaskUnknownException();
        taskRepository.deleteByUserIdAndName(userId, name);
        return new TaskDTO(task);
    }

    @NotNull
    @Override
    public List<TaskDTO> getAllTasksDTO() {
        @Nullable final List<Task> taskList = taskRepository.findAll();
        if (taskList == null || taskList.isEmpty()) throw new TaskUnknownException();
        return getTaskDTOS(taskList);
    }

    @NotNull
    @Override
    @Transactional
    public List<TaskDTO> loadTasks(@Nullable final List<TaskDTO> tasksDTO) {
        if (tasksDTO == null || tasksDTO.isEmpty()) throw new TaskUnknownException();
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (@NotNull final TaskDTO taskDTO : tasksDTO) {
            @NotNull final Task task = new Task(taskDTO);
            taskList.add(task);
        }
        taskRepository.saveAll(taskList);
        return tasksDTO;
    }

    @Override
    @Transactional
    public void removeAllTasks() {
        taskRepository.deleteAll();
    }

}