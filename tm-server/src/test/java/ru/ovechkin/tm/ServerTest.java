package ru.ovechkin.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ovechkin.tm.config.ServerConfiguration;
import ru.ovechkin.tm.service.PropertyService;


public class ServerTest {

    @NotNull
    final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @Test
    public void name() {
        System.out.println(context.getBean(PropertyService.class).getJdbcDriver());
        System.out.println(context.getBean(PropertyService.class).getJdbcPassword());
        System.out.println(context.getBean(PropertyService.class).getJdbcUrl());
        System.out.println(context.getBean(PropertyService.class).getJdbcUsername());
        System.out.println(context.getBean(PropertyService.class).getSessionSalt());
        System.out.println(context.getBean(PropertyService.class).getSessionCycle());
        System.out.println(context.getBean(PropertyService.class).getServiceHost());
    }
}