package ru.ovechkin.tm.listener.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.StorageEndpoint;

@Component
public class DataBase64LoadListener extends AbstractListener {

    @Autowired
    private StorageEndpoint storageEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_BASE64_LOAD;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from base64 file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64LoadListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[DATA BASE64 LOAD]");
        storageEndpoint.dataBase64Load(sessionDTO);
        System.out.println("[OK]");
    }

}