package ru.ovechkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

@Component
public final class HelpListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_HELP;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_HELP;
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands";
    }

    @NotNull
    @Autowired
    private AbstractListener[] abstractListeners;

    @Override
    @EventListener(condition = "@helpListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener command : abstractListeners) {
            System.out.println(command.name()
                    + (command.arg() != null ? ", " + command.arg() + " - " : " - ")
                    + command.description());
        }
        System.out.println("[OK]");
    }

}
