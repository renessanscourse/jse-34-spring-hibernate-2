package ru.ovechkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.TaskDTO;
import ru.ovechkin.tm.endpoint.TaskEndpoint;

import java.util.List;

@Component
public final class TaskListListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_TASK_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    @EventListener(condition = "@taskListListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[LIST TASKS]");
        @NotNull final List<TaskDTO> tasksDTO = taskEndpoint.findUserTasks(sessionDTO);
        @NotNull int index = 1;
        System.out.printf("%6s| %40s| %15s| %40s| %n", "NUMBER", "ID", "TASK_NAME", "PROJECT_ID");
        System.out.println("*********************************************************************");
        for (@NotNull final TaskDTO taskDTO : tasksDTO) {
            System.out.printf("%6s| %40s| %15s| %40s| %n", index, taskDTO.getId(), taskDTO.getName(), taskDTO.getProjectId());
            index++;
        }
        System.out.println("[OK]");
    }

}