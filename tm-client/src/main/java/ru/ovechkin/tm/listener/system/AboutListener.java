package ru.ovechkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

@Component
public final class AboutListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_ABOUT;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_ABOUT;
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info";
    }

    @Override
    @EventListener(condition = "@aboutListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println("NAME:\tOvechkin Roman");
        System.out.println("E-MAIL:\troman@ovechkin.ru");
    }

}