package ru.ovechkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectDTO;
import ru.ovechkin.tm.endpoint.ProjectEndpoint;

import java.util.List;

@Component
public final class ProjectListListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    @EventListener(condition = "@projectListListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[LIST PROJECT]");
        @NotNull final List<ProjectDTO> projectsDTO = projectEndpoint.findUserProjects(sessionDTO);
        int index = 1;
        System.out.printf("%6s| %37s| %15s|%n", "NUMBER", "ID", "PROJECT_NAME");
        System.out.println("*********************************************************************");
        for (final ProjectDTO projectDTO : projectsDTO) {
            System.out.printf("%6s| %37s| %15s|%n", index, projectDTO.getId(), projectDTO.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}