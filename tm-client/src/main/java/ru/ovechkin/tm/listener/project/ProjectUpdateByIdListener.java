package ru.ovechkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectDTO;
import ru.ovechkin.tm.endpoint.ProjectEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.PROJECT_UPDATE_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id";
    }

    @Override
    @EventListener(condition = "@projectUpdateByIdListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectDTO projectDTO = projectEndpoint.findProjectById(sessionDTO, id);
        if (projectDTO == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PROJECT DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectDTO projectUpdated =
                projectEndpoint.updateProjectById(sessionDTO, id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

}