package ru.ovechkin.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.endpoint.UserEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public class UserLockListener extends AbstractListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "lock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user's account";
    }

    @Override
    @EventListener(condition = "@userLockListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[LOCK USER]");
        System.out.print("ENTER USER'S LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        userEndpoint.lockUserByLogin(sessionDTO, login);
        System.out.println("[OK]");
    }

}