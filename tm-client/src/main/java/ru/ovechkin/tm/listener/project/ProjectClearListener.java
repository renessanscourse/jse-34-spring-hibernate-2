package ru.ovechkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectEndpoint;

@Component
public final class ProjectClearListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_CLEAR;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    @EventListener(condition = "@projectClearListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[CLEAR PROJECTS]");
        projectEndpoint.removeAllProjects(sessionDTO);
        System.out.println("[OK]");
    }

}