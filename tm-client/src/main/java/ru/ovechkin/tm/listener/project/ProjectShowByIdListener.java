package ru.ovechkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectDTO;
import ru.ovechkin.tm.endpoint.ProjectEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.PROJECT_SHOW_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id";
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectDTO projectDTO = projectEndpoint.findProjectById(sessionDTO, id);
        System.out.println("ID: " + projectDTO.getId());
        System.out.println("NAME: " + projectDTO.getName());
        System.out.println("DESCRIPTION: " + projectDTO.getDescription());
        System.out.println("[COMPLETE]");
    }

}