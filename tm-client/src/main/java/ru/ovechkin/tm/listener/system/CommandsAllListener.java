package ru.ovechkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

@Component
public final class CommandsAllListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_COMMANDS;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_COMMANDS;
    }

    @NotNull
    @Override
    public String description() {
        return "Show available commands";
    }

    @NotNull
    @Autowired
    private AbstractListener[] abstractListeners;

    @Override
    @EventListener(condition = "@commandsAllListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        for (@NotNull final AbstractListener command : abstractListeners)
            System.out.println(command.name());
    }

}