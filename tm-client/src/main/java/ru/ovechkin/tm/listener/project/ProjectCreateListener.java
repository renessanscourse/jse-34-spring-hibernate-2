package ru.ovechkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public final class ProjectCreateListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_CREATE;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[CREATE PROJECT]");
        System.out.print("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.nextLine();
        projectEndpoint.createProjectWithNameAndDescription(sessionDTO, name, description);
        System.out.println("[OK]");
    }

}