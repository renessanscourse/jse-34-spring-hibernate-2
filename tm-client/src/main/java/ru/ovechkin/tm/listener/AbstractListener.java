package ru.ovechkin.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.endpoint.SessionDTO;

@Component
public abstract class AbstractListener {

    @NotNull
    protected final static SessionDTO sessionDTO = new SessionDTO();

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void handle(final ConsoleEvent event);

    public AbstractListener() {
    }

}