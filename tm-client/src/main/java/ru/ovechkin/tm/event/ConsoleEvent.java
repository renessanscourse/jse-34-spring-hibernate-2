package ru.ovechkin.tm.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.bootstrap.Bootstrap;

@Component
public class ConsoleEvent extends ApplicationEvent {

    public ConsoleEvent(Bootstrap source) {
        super(source);
    }

    private String command;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

}
