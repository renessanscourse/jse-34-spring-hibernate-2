package ru.ovechkin.tm.exeption.empty;

public class LastNameEmptyException extends RuntimeException {

    public LastNameEmptyException() {
        super("Error! Last name is empty...");
    }

}