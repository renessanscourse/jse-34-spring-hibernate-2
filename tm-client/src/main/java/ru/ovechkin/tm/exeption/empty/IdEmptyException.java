package ru.ovechkin.tm.exeption.empty;

public class IdEmptyException extends RuntimeException {

    public IdEmptyException() {
        super("Error! ID is empty...");
    }

}